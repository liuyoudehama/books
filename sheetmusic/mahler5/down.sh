# !/bin/sh

#https://archives.nyphil.org/index.php/jp2/%7CMP%7C2%7C2176-111%7CMP_2176-111_001.jp2/portrait/1200

for (( i = 1; i <= 43; i++ )); do
	
	if [[ $i -lt 10 ]]; then
		NUM="0$i"
	else
		NUM=$i
	fi
	
	URL="https://archives.nyphil.org/index.php/jp2/%7CMP%7C2%7C2176-111%7CMP_2176-111_0$NUM.jp2/portrait/1200"
	curl $URL > $NUM.jpeg	
done

magick *.jpeg mahler5_trp.pdf