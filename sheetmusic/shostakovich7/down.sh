# !/bin/sh

for (( i = 1; i <= 184; i++ )); do
	if [[ $i -lt 10 ]]; then
		page_num="00$i"
	elif [[ $i -lt 100 ]]; then
		page_num="0$i"
	else
		page_num="$i"
	fi
	URL="https://archives.nyphil.org/index.php/jp2/|MS|2|2858|MS_2858_${page_num}.jp2/landscape/1500"
	curl $URL > $page_num.jpeg	
done

magick `ls *.jpeg` shostakovich7_complete.pdf
