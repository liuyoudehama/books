# !/bin/sh

for (( i = 1; i <= 53; i++ )); do
	if [[ $i -lt 10 ]]; then
		page_num="00$i"
	elif [[ $i -lt 100 ]]; then
		page_num="0$i"
	else
		page_num="$i"
	fi
	URL="https://archives.nyphil.org/index.php/jp2/|MP|1|1820-111|MP_1820-111_001.jp2/portrait/1200"
	curl $URL > $page_num.jpeg	
done

magick `ls *.jpeg` shostakovich7_trp.pdf
